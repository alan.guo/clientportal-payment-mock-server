# Clientportal Payment Mock Server

## How to run  
To run the ClientPortal Payment Mock Server using the provided code, follow these steps:
1. Connect to the server via SSH:
   ```bash
   ssh YOUR_ACCOUNT@SERVER_IP
   ```
2. Navigate to the mock server directory:
   ```bash
   cd /opt/mockserver
   ```
3. Copy the Dockerfile to your home directory:
   ```bash
   cp server/clientportal-payment-mock-server/Dockerfile ~/Dockerfile
   ```
4. Build the Docker image:
   ```bash
   docker build -t payimg .
   ```
5. Run the Docker container:
   ```bash
   docker run --name payct -d --rm -p 8000:8000 payimg
   ```
After running these commands, the ClientPortal Payment Mock Server will be up and running on port 8000 of your server.

Please note that the provided code assumes that you have already set up the necessary dependencies and configurations for the FastAPI application. Make sure to install the required packages and set up any additional environment variables or configurations as needed.

## API Routes
The ClientPortal Payment Mock Server exposes the following API routes:

### PlusDebit Mock Deposit P2P
- Endpoint: /pd/deposit/p2p
- Method: POST
- Description: Simulates a PlusDebit mock deposit P2P request.
- Returns: The generated payload for the callback.
### PlusDebit Mock Page
- Endpoint: /plusdebit_mock_page
- Method: GET
- Description: Returns the mock page for PlusDebit.
- Returns: The rendered PlusDebit mock page HTML.
### PlusDebit Callback
- Endpoint: /plusdebit_callback
- Method: POST
- Description: Simulates a callback from PlusDebit.
- Returns: The rendered PlusDebit mock page result HTML.

### PaymentAsia Mock App Page (Merchant Token)
- Endpoint: /app/page/{PAYMENT_ASIA_MERCHANT_TOKEN}
- Method: POST
- Description: Simulates a mock app page request for PaymentAsia with a specific - merchant token.
- Returns: A redirect response to the specified return URL.
### PaymentAsia Mock Page
- Endpoint: /paymentasia_mock_page
- Method: GET
- Description: Returns the mock page for PaymentAsia.
- Returns: The rendered PaymentAsia mock page HTML.
### PaymentAsia Mock Merchant Token Payment Query
- Endpoint: /{PAYMENT_ASIA_MERCHANT_TOKEN}/payment/query
- Method: POST
- Description: Simulates a payment query request for a specific merchant token.
- Returns: The response data for the payment query.

### Templates
The ClientPortal Payment Mock Server uses the following templates located in the `templates` directory:
- FastAPIPages.html: Template for the home page.
- sticpay_mock_page.html: Template for the Sticpay mock page.
- sticpay_mock_page_result.html: Template for the Sticpay mock page result.
- plusdebit_mock_page.html: Template for the PlusDebit mock page.
- plusdebit_mock_page_result.html: Template for the PlusDebit mock page result.
- paymentasia_mock_page.html: Template for the PaymentAsia mock page.

Make sure to provide the necessary template files in the `templates` directory for the server to render the pages correctly.
